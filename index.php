<?php
//     Copyright (c) 2012 Sune Vuorela <sune@vuorela.dk>
//
//    Permission is hereby granted, free of charge, to any person
//    obtaining a copy of this software and associated documentation
//    files (the "Software"), to deal in the Software without
//    restriction, including without limitation the rights to use,
//    copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the
//    Software is furnished to do so, subject to the following
//    conditions:
//
//    The above copyright notice and this permission notice shall be
//    included in all copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//    OTHER DEALINGS IN THE SOFTWARE.
date_default_timezone_set("UTC");

include_once('simplepie-1.3/autoloader.php');

include_once('config.php');

$base_youtube_url = "http://gdata.youtube.com/feeds/base/users/@@USER@@/uploads?alt=rss&v=2&orderby=published";

$app_feed_urls = array();
foreach($youtube_users as $youtube_user) {
  $app_feed_urls[] = str_replace("@@USER@@",$youtube_user,$base_youtube_url);
}

$app_feed_urls = array_merge($app_feed_urls, $feed_urls);

class content {
  private $m_title;
  private $m_link;
  private $m_description;
  private $m_author;
  private $m_items = array();
  function set_description($description) {
    $this->m_description = $description;
  }
  function set_title($title) {
    $this->m_title = $title;
  }
  function set_link($link) {
    $this->m_link = $link;
  }
  function set_author($author) {
    $this->m_author = $author;
  }
  function title() {
    return $this->m_title;
  }
  function link() {
    return $this->m_link;
  }
  function add_item($item) {
    $this->m_items[] = $item;
  }
  function item_count() {
    return count($this->m_items);
  }
  function items() {
    return $this->m_items;
  }
  function description() {
    return $this->m_description;
  }
  function author() {
    return $this->m_author;
  }
}
class item {
  private $m_title;
  private $m_description;
  private $m_link;
  private $m_author;
  private $m_date;
  function set_author($author) {
    $this->m_author = $author;
  }
  function set_title($title) {
    $this->m_title = $title;
  }
  function set_description($description) {
    $this->m_description = $description;
  }
  function set_date($date) {
    $this->m_date = $date;
  }
  function set_link($link) {
    $this->m_link = $link;
  }
  function author() {
    return $this->m_author;
  }
  function link() {
    return $this->m_link;
  }
  function description() {
    return $this->m_description;
  }
  function title() {
    return $this->m_title;
  }
  function date() {
    return $this->m_date;
  }
}

$contents = array();

foreach($app_feed_urls as $url ) {
  $rss = new SimplePie();
  $rss->enable_cache(false);
  $rss->set_feed_url($url);
  $rss->set_stupidly_fast(true);
  if(!$rss->init()) {
    echo $rss->error();
    continue;
  }
  $rss->handle_content_type();
  $content = new content();

  foreach($rss->get_items(0,5) as $rssitem) {
    $item = new item();
    $item->set_author($rssitem->get_author()->get_email()); //dunno if it is weird feeds I*m using or if there is a bug in using emails
    $item->set_description($rssitem->get_description());
    $item->set_title($rssitem->get_title());
    $item->set_date($rssitem->get_date());
    $item->set_link($rssitem->get_permalink());
    $content->add_item($item);
  }
  $content->set_description($rss->get_description());
  $content->set_link($rss->get_permalink());
  $content->set_title($rss->get_title());
  $contents[] = $content;
  unset($rss);
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <title>My RSS Feeds</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon" href="images/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png" />
  <style>
    a {
      text-decoration: none;
      color: #003BC3;
    }
    a:hover {
      text-decoration: underline;
    }
    body {
      font-size: 11px;
      font-family: sans-serif;
    }
    #by {
      clear:both;
      text-align: center;
    }
    .feed {
      float: left;
      border: 1px solid;
      margin: 5px;
      background: #eeeeee;
    }
    .feed h2 {
      margin-left: 8px;
      margin-right: 8px;
    }
  </style>
</head>
<body>
<div>
  <h1>RSS Feeds
    <div style="float: right">
      <img src="images/logo.png" />
    </div>
  </h1>
</div>
<?php

foreach($contents as $feed) {
  ?>
  <div class="feed">
  <h2><a href="<?php echo $feed->link();?>"><?php echo $feed->title();?></a></h2>
  <ul>
    <?php
      foreach($feed->items() as $item) {
        ?>
        <li>
          <a href="<?php echo $item->link();?>"><?php echo $item->title();?></a>
          <p><?php echo $item->author();?> -- <?php echo $item->date();?></p>
        </li>
        <?php
      }
    ?>
  </ul>
  </div>
  <?php
}

?>
  <div id="by"><hr />Powered by <a href="http://sune.vuorela.dk/kadaka">Kadaka</a> by <a href="http://sune.vuorela.dk">Sune Vuorela</a></div>
  </div>

</body>
</html>

<?php



?>